const sum = (a,b) => a + b;

const express = require('express');

// The code below will display 'Hello World!' to the browser when you go to http://localhost:3000 
const app = express();   
app.get('/', function (req, res) {   res.send('Hello World!') });   
const server = app.listen(3000, function () { console.log('Example app listening on port 3000!') });   
module.exports = { app, sum };
module.exports.server = server; // export the server so we can close it in the jest test