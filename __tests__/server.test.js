const request = require('supertest');
const {app,server,sum} = require('../server');

test('Adding 2 + 3 equals 5', () => {
    expect(sum(2,3)).toBe(5)
})

describe('GET /', () => {   
    test('displays "Hello World!"', done => {
        // The line below is the core test of our app.     
        request(app).get('/').expect('Hello World!', done);
    });
});  

afterAll(async () => {
    try {
        await server.close()
    } catch (error) {
        console.error(error);
        throw error;
    }
});